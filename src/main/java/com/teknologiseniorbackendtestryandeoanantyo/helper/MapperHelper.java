package com.teknologiseniorbackendtestryandeoanantyo.helper;

import com.teknologiseniorbackendtestryandeoanantyo.model.*;
import com.teknologiseniorbackendtestryandeoanantyo.model.entity.BusinessCategoryEntity;
import com.teknologiseniorbackendtestryandeoanantyo.model.entity.BusinessEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MapperHelper {
    public static Business mapBEtoBusiness(BusinessEntity be, List<Category> categories){
        Business business = new Business();
        business.setId(be.getBusiness_id());
        business.setAlias(be.getAlias());
        business.setName(be.getName());
        business.setImage_url(be.getImage_url());
        business.setIs_closed(be.getIs_closed());
        business.setUrl(be.getUrl());
        business.setReview_count(be.getReview_count());
        business.setCategories(categories);
        business.setRating(be.getRating());
        business.setCoordinates(mapBEtoCoordinates(be));
        business.setTransactions(be.getTransactions());
        business.setPrice(be.getPrice());
        business.setLocation(mapBEtoLocation(be));
        business.setPhone(be.getPhone());
        business.setDisplay_phone(be.getDisplay_phone());
        business.setDistance(be.getDistance());
        business.setHours(mapBEtoHours(be));
        business.setAttributes(be.getAttributes());
        business.setTotal(be.getTotal());
        return business;
    }

    public static Category mapBCEtoCategory(BusinessCategoryEntity bce){
        Category category = new Category();
        category.setAlias(bce.getAlias());
        category.setTitle(bce.getTitle());
        return category;
    }

    public static Coordinates mapBEtoCoordinates(BusinessEntity be){
        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude(be.getLatitude().toString());
        coordinates.setLongitude(be.getLongitude().toString());
        return coordinates;
    }

    public static Location mapBEtoLocation(BusinessEntity be){
        Location location = new Location();
        location.setAddress1(be.getAddress1());
        location.setAddress2(be.getAddress2());
        location.setAddress3(be.getAddress3());
        location.setCity(be.getCity());
        location.setZip_code(be.getZip_code());
        location.setCountry(be.getCountry());
        location.setState(be.getState());
        location.setDisplay_address(be.getDisplay_address());
        location.setCross_streets(be.getCross_streets());
        return location;
    }

    public static Hours mapBEtoHours(BusinessEntity be){
        Hours hours = new Hours();
        hours.setHour_type(be.getHour_type());
        List<OpenHours> openHoursList = new ArrayList<>();
        OpenHours openHours = new OpenHours();
        openHours.setDay(be.getDayB());
        openHours.setStart(be.getStartB());
        openHours.setEnd(be.getEndB());
        openHours.setIs_overnight(be.getIs_overnight());
        openHours.setIs_open_now(be.getIs_open_now());
        openHoursList.add(openHours);
        hours.setOpen(openHoursList);
        return hours;
    }

    public static BusinessEntity mapBusinessToBE(Business business){
        BusinessEntity be = new BusinessEntity();
        be.setAlias(business.getAlias());
        be.setName(business.getName());
        be.setImage_url(business.getImage_url());
        be.setIs_closed(business.getIs_closed());
        be.setUrl(business.getUrl());
        be.setReview_count(business.getReview_count());
        be.setRating(business.getRating());
        be.setLatitude(new BigDecimal(business.getCoordinates().getLatitude()));
        be.setLongitude(new BigDecimal(business.getCoordinates().getLongitude()));
        be.setTransactions(business.getTransactions());
        be.setPrice(business.getPrice());

        Location loc = business.getLocation();
        be.setAddress1(loc.getAddress1());
        be.setAddress2(loc.getAddress2());
        be.setAddress3(loc.getAddress3());
        be.setCity(loc.getCity());
        be.setZip_code(loc.getZip_code());
        be.setCountry(loc.getCountry());
        be.setState(loc.getState());
        be.setDisplay_address(loc.getDisplay_address());
        be.setCross_streets(loc.getCross_streets());

        be.setPhone(business.getPhone());
        be.setDisplay_phone(business.getDisplay_phone());
        be.setDistance(business.getDistance());
        be.setHour_type(business.getHours().getHour_type());

        OpenHours oh = business.getHours().getOpen().get(0);
        be.setDayB(oh.getDay());
        be.setStartB(oh.getStart());
        be.setEndB(oh.getEnd());
        be.setIs_overnight(oh.getIs_overnight());
        be.setIs_open_now(oh.getIs_open_now());

        be.setAttributes(business.getAttributes());
        be.setTotal(business.getTotal());

        return be;
    }

    public static BusinessCategoryEntity mapBusinessCategoryToBCE(Integer businessId, Category category){
        BusinessCategoryEntity bce = new BusinessCategoryEntity();
        bce.setBusiness_id(businessId);
        bce.setAlias(category.getAlias());
        bce.setTitle(category.getTitle());
        return bce;
    }
}
