package com.teknologiseniorbackendtestryandeoanantyo.service;

import com.teknologiseniorbackendtestryandeoanantyo.helper.MapperHelper;
import com.teknologiseniorbackendtestryandeoanantyo.model.Business;
import com.teknologiseniorbackendtestryandeoanantyo.model.Category;
import com.teknologiseniorbackendtestryandeoanantyo.model.entity.BusinessCategoryEntity;
import com.teknologiseniorbackendtestryandeoanantyo.model.entity.BusinessEntity;
import com.teknologiseniorbackendtestryandeoanantyo.model.request.ReqBusiness;
import com.teknologiseniorbackendtestryandeoanantyo.repository.IBusinessCategoryRepository;
import com.teknologiseniorbackendtestryandeoanantyo.repository.IBusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class BusinessService {
    @Autowired
    IBusinessRepository businessRepository;

    @Autowired
    IBusinessCategoryRepository businessCategoryRepository;

    public List<Business> findBusinessByFilter(ReqBusiness reqBusiness){
        List<Business> businesses = new ArrayList<>();
        //filter categories
        String reqCategories = reqBusiness.getCategories() == null ? "": reqBusiness.getCategories();
        String[] arrSplitCategories = reqCategories.trim().split("\\s*,\\s*");
        List<String> filterCategories = Arrays.asList(arrSplitCategories);

        List<Category> categories = new ArrayList<>();
        List<BusinessCategoryEntity> businessCategoryEntities = businessCategoryRepository.findByFilter(filterCategories);
        for(int i=0; i<businessCategoryEntities.size(); i++){
            BusinessCategoryEntity bce = businessCategoryEntities.get(i);
            Integer currBusinessId = bce.getBusiness_id();
            Integer prevBusinessId = i == 0 ? 0 : businessCategoryEntities.get(i-1).getBusiness_id();
            Integer nextBusinessId = i == businessCategoryEntities.size()-1 ? 0 : businessCategoryEntities.get(i+1).getBusiness_id();
            //refresh list categories for next businessId
            if(currBusinessId != prevBusinessId){
                categories.removeAll(categories);
            }

            Category category = MapperHelper.mapBCEtoCategory(bce);
            categories.add(category);

            //add categories to business
            if(currBusinessId != nextBusinessId){
                //filter location, latitude, longitude, term
                String location = reqBusiness.getLocation() == null ? "": reqBusiness.getLocation() ;
                BigDecimal latitude = reqBusiness.getLatitude() == null ? new BigDecimal(0.0): reqBusiness.getLatitude();
                BigDecimal longitude = reqBusiness.getLongitude() == null ? new BigDecimal(0.0): reqBusiness.getLongitude();
                String term = reqBusiness.getTerm() == null ? "": reqBusiness.getTerm();
                Optional<BusinessEntity> be = businessRepository.findByFilter(currBusinessId, location, latitude, longitude, term);
                if(be.isPresent()) {
                    Business business = MapperHelper.mapBEtoBusiness(be.get(), categories);
                    businesses.add(business);
                }
            }
        }

        return businesses;
    }

    public Business saveBusiness(Business business){
        try {
            BusinessEntity be = MapperHelper.mapBusinessToBE(business);
            be = businessRepository.save(be);
            business.setId(be.getBusiness_id());
            return business;
        } catch (Exception e){}
        return new Business();
    }

    public void saveBusinessCategory(Integer businessID, List<Category> categories){
        categories.forEach(category -> {
            BusinessCategoryEntity bce = MapperHelper.mapBusinessCategoryToBCE(businessID, category);
            businessCategoryRepository.save(bce);
        });
    }

    public Business updateBusiness(Business business){
        try {
            Optional<BusinessEntity> oldBE = businessRepository.findById(business.getId());
            List<BusinessCategoryEntity> oldListBCE = businessCategoryRepository.findByBusinessId(business.getId());
            if(oldBE.isPresent() && oldListBCE.size()>0){
                //delete old Business Entity
                businessRepository.delete(oldBE.get());
                //delete old Business Category Entity
                oldListBCE.forEach(oldBCE->{
                    businessCategoryRepository.delete(oldBCE);
                });

                //new Business Entity
                BusinessEntity newBE = MapperHelper.mapBusinessToBE(business);
                newBE = businessRepository.save(newBE);
                business.setId(newBE.getBusiness_id());
                //new Business Category Entity
                List<Category> newListCategory = business.getCategories();
                newListCategory.forEach(newCategory -> {
                    BusinessCategoryEntity bce = MapperHelper.mapBusinessCategoryToBCE(business.getId(), newCategory);
                    businessCategoryRepository.save(bce);
                });
            }
            return business;
        } catch (Exception e){}
        return new Business();
    }

    public void deleteBusiness(Integer businessId){
        try {
            Optional<BusinessEntity> be = businessRepository.findById(businessId);
            List<BusinessCategoryEntity> listBCE = businessCategoryRepository.findByBusinessId(businessId);
            if(be.isPresent() && listBCE.size()>0) {
                //delete Business Entity
                businessRepository.delete(be.get());
                //delete Business Category Entity
                listBCE.forEach(oldBCE -> {
                    businessCategoryRepository.delete(oldBCE);
                });
            }
        }catch (Exception e){}
    }
}
