package com.teknologiseniorbackendtestryandeoanantyo.repository;

import com.teknologiseniorbackendtestryandeoanantyo.model.entity.BusinessEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Optional;

public interface IBusinessRepository extends JpaRepository<BusinessEntity, Integer> {
    @Query(value = "select * from tbl_business b " +
            "where b.business_id = :business_id " +
            "and b.display_address like %:location% " +
            "and (b.latitude = :latitude or 0.0 = :latitude) " +
            "and (b.longitude = :longitude or 0.0 = :longitude) " +
            "and b.name like %:term%", nativeQuery = true)
    public Optional<BusinessEntity> findByFilter(
            @Param("business_id") Integer business_id, @Param("location") String location,
            @Param("latitude") BigDecimal latitude, @Param("longitude") BigDecimal longitude,
            @Param("term") String term
    );
}
