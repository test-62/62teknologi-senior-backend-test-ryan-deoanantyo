package com.teknologiseniorbackendtestryandeoanantyo.repository;

import com.teknologiseniorbackendtestryandeoanantyo.model.entity.BusinessCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IBusinessCategoryRepository extends JpaRepository<BusinessCategoryEntity, Integer> {
    @Query(value = "select bc.business_category_id, bc.business_id, bc.alias, c.title " +
            "from tbl_business_category bc " +
            "inner join tbl_category c on bc.alias = c.alias " +
            "where bc.alias in (:categories) or \'\' in (:categories) " +
            "order by bc.business_id, bc.business_category_id", nativeQuery = true)
    public List<BusinessCategoryEntity> findByFilter(@Param("categories") List<String> categories);

    @Query(value = "select bc.business_category_id, bc.business_id, bc.alias, bc.title " +
            "from tbl_business_category bc " +
            "where bc.business_id = :business_id", nativeQuery = true)
    public List<BusinessCategoryEntity> findByBusinessId(@Param("business_id") Integer business_id);
}
