package com.teknologiseniorbackendtestryandeoanantyo.repository;

import com.teknologiseniorbackendtestryandeoanantyo.model.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICategoryRepository extends JpaRepository<CategoryEntity, String> {
}
