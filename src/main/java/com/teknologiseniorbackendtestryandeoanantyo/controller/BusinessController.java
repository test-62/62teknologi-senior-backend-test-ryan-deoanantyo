package com.teknologiseniorbackendtestryandeoanantyo.controller;

import com.google.gson.Gson;
import com.teknologiseniorbackendtestryandeoanantyo.model.Business;
import com.teknologiseniorbackendtestryandeoanantyo.model.Category;
import com.teknologiseniorbackendtestryandeoanantyo.model.LBusiness;
import com.teknologiseniorbackendtestryandeoanantyo.model.request.ReqBusiness;
import com.teknologiseniorbackendtestryandeoanantyo.model.response.StdResponse;
import com.teknologiseniorbackendtestryandeoanantyo.service.BusinessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("business")
public class BusinessController {
    private static Logger logger = LoggerFactory.getLogger(BusinessController.class);

    private static Gson gson = new Gson();

    @Autowired
    BusinessService businessService;

    @GetMapping("/search")
    public ResponseEntity<StdResponse<LBusiness>> findBusinessByFilter(@RequestBody ReqBusiness reqBusiness){
        logger.info("=== start findBusinessByFilter === with filter: " + gson.toJson(reqBusiness));
        LBusiness lBusiness= new LBusiness();
        List<Business> businesses = businessService.findBusinessByFilter(reqBusiness);
        lBusiness.setBusinesses(businesses);
        StdResponse<LBusiness> response = new StdResponse<>(HttpStatus.OK.toString(),"",lBusiness);
        logger.info("=== end findBusinessByFilter ===");
        return new ResponseEntity<StdResponse<LBusiness>>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<StdResponse<Business>> saveBusiness(@RequestBody Business business){
        logger.info("=== start saveBusiness === with business: " + gson.toJson(business));
        business = businessService.saveBusiness(business);
        List<Category> categories = business.getCategories();
        if(categories.size() > 0){
            businessService.saveBusinessCategory(business.getId(), categories);
        }
        StdResponse<Business> response = new StdResponse<>(HttpStatus.OK.toString(),"",business);
        logger.info("=== end saveBusiness ===");
        return new ResponseEntity<StdResponse<Business>>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<StdResponse<Business>> updateBusiness(@RequestBody Business business){
        logger.info("=== start updateBusiness === with business: " + gson.toJson(business));
        business = businessService.updateBusiness(business);
        StdResponse<Business> response = new StdResponse<>(HttpStatus.OK.toString(),"",business);
        logger.info("=== end updateBusiness ===");
        return new ResponseEntity<StdResponse<Business>>(response, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<StdResponse<Integer>> deleteBusiness(@RequestParam Integer businessId){
        logger.info("=== start deleteBusiness === with businessId: " + gson.toJson(businessId));
        businessService.deleteBusiness(businessId);
        StdResponse<Integer> response = new StdResponse<>(HttpStatus.OK.toString(),"",businessId);
        logger.info("=== end deleteBusiness ===");
        return new ResponseEntity<StdResponse<Integer>>(response, HttpStatus.OK);
    }
}
