package com.teknologiseniorbackendtestryandeoanantyo.model.request;

import java.math.BigDecimal;

public class ReqBusiness {
    private String location;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private String term;
    private Integer radius;
    private String categories;
    private String locale;
    private Integer price;
    private Boolean open_now;
    private Integer open_at;
    private String attributes;
    private String sort_by;
    private String device_platform;
    private String reservation_date;
    private String reservation_time;
    private Integer reservation_covers;
    private Boolean matches_party_size_param;
    private Integer limit;
    private Integer offset;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Boolean getOpen_now() {
        return open_now;
    }

    public void setOpen_now(Boolean open_now) {
        this.open_now = open_now;
    }

    public Integer getOpen_at() {
        return open_at;
    }

    public void setOpen_at(Integer open_at) {
        this.open_at = open_at;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getSort_by() {
        return sort_by;
    }

    public void setSort_by(String sort_by) {
        this.sort_by = sort_by;
    }

    public String getDevice_platform() {
        return device_platform;
    }

    public void setDevice_platform(String device_platform) {
        this.device_platform = device_platform;
    }

    public String getReservation_date() {
        return reservation_date;
    }

    public void setReservation_date(String reservation_date) {
        this.reservation_date = reservation_date;
    }

    public String getReservation_time() {
        return reservation_time;
    }

    public void setReservation_time(String reservation_time) {
        this.reservation_time = reservation_time;
    }

    public Integer getReservation_covers() {
        return reservation_covers;
    }

    public void setReservation_covers(Integer reservation_covers) {
        this.reservation_covers = reservation_covers;
    }

    public Boolean getMatches_party_size_param() {
        return matches_party_size_param;
    }

    public void setMatches_party_size_param(Boolean matches_party_size_param) {
        this.matches_party_size_param = matches_party_size_param;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}
