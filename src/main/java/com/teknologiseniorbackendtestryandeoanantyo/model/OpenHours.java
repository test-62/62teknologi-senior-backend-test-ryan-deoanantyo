package com.teknologiseniorbackendtestryandeoanantyo.model;

public class OpenHours {
    private Integer day;
    private String start;
    private String end;
    private Boolean is_overnight;
    private Boolean is_open_now;

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Boolean getIs_overnight() {
        return is_overnight;
    }

    public void setIs_overnight(Boolean is_overnight) {
        this.is_overnight = is_overnight;
    }

    public Boolean getIs_open_now() {
        return is_open_now;
    }

    public void setIs_open_now(Boolean is_open_now) {
        this.is_open_now = is_open_now;
    }
}
