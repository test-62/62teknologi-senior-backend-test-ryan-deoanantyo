package com.teknologiseniorbackendtestryandeoanantyo.model.entity;

import jakarta.persistence.*;

import java.math.BigDecimal;

@Entity
@Table(name = "tbl_business")
public class BusinessEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "business_id")
    private Integer business_id;
    @Column(name = "alias")
    private String alias;
    @Column(name = "name")
    private String name;
    @Column(name = "image_url")
    private String image_url;
    @Column(name = "is_closed", columnDefinition="BIT")
    private Boolean is_closed;
    @Column(name = "url")
    private String url;
    @Column(name = "review_count")
    private String review_count;
    @Column(name = "rating")
    private String rating;
    @Column(name = "latitude", precision = 10, scale = 6)
    private BigDecimal latitude;
    @Column(name = "longitude", precision = 10, scale = 6)
    private BigDecimal longitude;
    @Column(name = "transactions")
    private String transactions;
    @Column(name = "price")
    private String price;
    @Column(name = "address1")
    private String address1;
    @Column(name = "address2")
    private String address2;
    @Column(name = "address3")
    private String address3;
    @Column(name = "city")
    private String city;
    @Column(name = "zip_code")
    private String zip_code;
    @Column(name = "country")
    private String country;
    @Column(name = "state")
    private String state;
    @Column(name = "display_address")
    private String display_address;
    @Column(name = "cross_streets")
    private String cross_streets;
    @Column(name = "phone")
    private String phone;
    @Column(name = "display_phone")
    private String display_phone;
    @Column(name = "distance")
    private String distance;
    @Column(name = "hour_type")
    private String hour_type;
    @Column(name = "dayB")
    private Integer dayB;
    @Column(name = "startB")
    private String startB;
    @Column(name = "endB")
    private String endB;
    @Column(name = "is_overnight", columnDefinition="BIT")
    private Boolean is_overnight;
    @Column(name = "is_open_now", columnDefinition="BIT")
    private Boolean is_open_now;
    @Column(name = "attributes")
    private String attributes;
    @Column(name = "total")
    private String total;

    public Integer getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(Integer business_id) {
        this.business_id = business_id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public Boolean getIs_closed() {
        return is_closed;
    }

    public void setIs_closed(Boolean is_closed) {
        this.is_closed = is_closed;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReview_count() {
        return review_count;
    }

    public void setReview_count(String review_count) {
        this.review_count = review_count;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public String getTransactions() {
        return transactions;
    }

    public void setTransactions(String transactions) {
        this.transactions = transactions;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDisplay_address() {
        return display_address;
    }

    public void setDisplay_address(String display_address) {
        this.display_address = display_address;
    }

    public String getCross_streets() {
        return cross_streets;
    }

    public void setCross_streets(String cross_streets) {
        this.cross_streets = cross_streets;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDisplay_phone() {
        return display_phone;
    }

    public void setDisplay_phone(String display_phone) {
        this.display_phone = display_phone;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getHour_type() {
        return hour_type;
    }

    public void setHour_type(String hour_type) {
        this.hour_type = hour_type;
    }

    public Integer getDayB() {
        return dayB;
    }

    public void setDayB(Integer dayB) {
        this.dayB = dayB;
    }

    public String getStartB() {
        return startB;
    }

    public void setStartB(String startB) {
        this.startB = startB;
    }

    public String getEndB() {
        return endB;
    }

    public void setEndB(String endB) {
        this.endB = endB;
    }

    public Boolean getIs_overnight() {
        return is_overnight;
    }

    public void setIs_overnight(Boolean is_overnight) {
        this.is_overnight = is_overnight;
    }

    public Boolean getIs_open_now() {
        return is_open_now;
    }

    public void setIs_open_now(Boolean is_open_now) {
        this.is_open_now = is_open_now;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
