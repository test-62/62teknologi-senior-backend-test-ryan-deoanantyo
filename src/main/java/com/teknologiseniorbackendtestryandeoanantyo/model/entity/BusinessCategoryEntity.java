package com.teknologiseniorbackendtestryandeoanantyo.model.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "tbl_business_category")
public class BusinessCategoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "business_category_id")
    private Integer business_category_id;
    @Column(name = "business_id")
    private Integer business_id;
    @Column(name = "alias")
    private String alias;
    @Column(name = "title")
    private String title;

    public Integer getBusiness_category_id() {
        return business_category_id;
    }

    public void setBusiness_category_id(Integer business_category_id) {
        this.business_category_id = business_category_id;
    }

    public Integer getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(Integer business_id) {
        this.business_id = business_id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
