package com.teknologiseniorbackendtestryandeoanantyo.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "tbl_category")
public class CategoryEntity {
    @Id
    @Column(name = "alias")
    private String alias;
    @Column(name = "title")
    private String title;
    @Column(name = "parent_aliases")
    private String parent_aliases;
    @Column(name = "country_whitelist")
    private String country_whitelist;
    @Column(name = "country_blacklist")
    private String country_blacklist;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParent_aliases() {
        return parent_aliases;
    }

    public void setParent_aliases(String parent_aliases) {
        this.parent_aliases = parent_aliases;
    }

    public String getCountry_whitelist() {
        return country_whitelist;
    }

    public void setCountry_whitelist(String country_whitelist) {
        this.country_whitelist = country_whitelist;
    }

    public String getCountry_blacklist() {
        return country_blacklist;
    }

    public void setCountry_blacklist(String country_blacklist) {
        this.country_blacklist = country_blacklist;
    }
}
