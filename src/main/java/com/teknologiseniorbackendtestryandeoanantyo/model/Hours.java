package com.teknologiseniorbackendtestryandeoanantyo.model;

import java.util.List;

public class Hours {
    private String hour_type;
    private List<OpenHours> open;

    public String getHour_type() {
        return hour_type;
    }

    public void setHour_type(String hour_type) {
        this.hour_type = hour_type;
    }

    public List<OpenHours> getOpen() {
        return open;
    }

    public void setOpen(List<OpenHours> open) {
        this.open = open;
    }
}
