package com.teknologiseniorbackendtestryandeoanantyo.model;

import java.util.List;

public class LBusiness {
    List<Business> businesses;

    public List<Business> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(List<Business> businesses) {
        this.businesses = businesses;
    }
}
