CREATE DATABASE db_business;

use db_business;

/*table tbl_business*/
CREATE TABLE tbl_business (
	business_id INT NOT NULL AUTO_INCREMENT,
	alias VARCHAR(200) NOT NULL,
	name VARCHAR(200) NOT NULL,
	image_url MEDIUMTEXT NOT NULL,
	is_closed BIT NOT NULL,
	url MEDIUMTEXT NOT NULL,
	review_count VARCHAR(20) NOT NULL,
	rating VARCHAR(5) NOT NULL,
	latitude DECIMAL(10,6) NOT NULL,
	longitude DECIMAL(10,6) NOT NULL,
	transactions VARCHAR(30) NOT NULL,
	price VARCHAR(5) NOT NULL,
	address1 VARCHAR(200) NULL,
	address2 VARCHAR(200) NULL,
	address3 VARCHAR(200) NULL,
	city VARCHAR(30) NULL,
	zip_code VARCHAR(10) NULL,
	country VARCHAR(2) NULL,
	state VARCHAR(2) NULL,
	display_address VARCHAR(600) NULL,
	cross_streets VARCHAR(50) NULL,
	phone VARCHAR(15) NOT NULL,
	display_phone VARCHAR(15) NOT NULL,
	distance VARCHAR(10) NULL,
	hour_type VARCHAR(10) NOT NULL,
	dayB INT NOT NULL,
	startB VARCHAR(4) NOT NULL,
	endB VARCHAR(4) NOT NULL,
	is_overnight BIT NOT NULL,
	is_open_now BIT NOT NULL,
	attributes VARCHAR(50) NULL,
	total VARCHAR(10) NOT NULL,
    PRIMARY KEY (business_id)
);

select * from tbl_business;

drop table tbl_business;

INSERT INTO tbl_business(
	alias, name, image_url, is_closed, url,
	review_count, rating, latitude, longitude, transactions, price,
	address1, address2, address3, city, zip_code, country, state, display_address, cross_streets, phone, display_phone, distance,
	hour_type, dayB, startB, endB, is_overnight, is_open_now, attributes, total
)VALUES (
	'yelp-manchester-stadion', 'Stadion City of Manchester', 'http://manchester-stadion-photo.com', 0, 'http://manchester-stadion.com', 
	100000, 5, 53.483139, -2.2003, 'delivery', '200', 
	'Sportcity Manchester M11 3FF', NULL, NULL, 'Manchester', '212', 'GB', 'GB', 'Sportcity Manchester M11 3FF', NULL, '054321', '054321', NULL, 
	'8 to 21', 5, '0800', '2100', 0, 1, 'hot_and_new', '10000' 
);

/*table tbl_business_category*/
CREATE TABLE tbl_business_category (
	business_category_id INT NOT NULL AUTO_INCREMENT,
	business_id INT NOT NULL,
	alias VARCHAR(50) NOT NULL,
	title mediumtext not null,
	PRIMARY KEY (business_category_id)
);

select * from tbl_business_category;

drop table tbl_business_category;

insert into tbl_business_category
(business_id, alias)
values
(1, 'football'),
(1, 'musicvenues');

/*table tbl_category*/
create table tbl_category(
	alias VARCHAR(50) NOT NULL,
	title mediumtext not null,
	parent_aliases mediumtext null,
	country_whitelist mediumtext null,
	country_blacklist mediumtext null,
	PRIMARY KEY (alias)
);

select * from tbl_category;

drop table tbl_category;

insert into tbl_category values
('football','Soccer', 'active', null, null),
('musicvenues','Music Venues', 'arts', null, null);

/*combine*/
select * from tbl_business;
select * from tbl_business_category;
select * from tbl_category;

select bc.business_category_id, bc.business_id, bc.alias, c.title 
from tbl_business_category bc
inner join tbl_category c on bc.alias = c.alias
where bc.alias in ('') or '' in ('')
order by bc.business_id, bc.business_category_id

select * from tbl_business b 
where b.business_id = 1
and b.display_address like '%stadion%'